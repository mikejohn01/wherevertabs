package com.wet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WETApplication {
    public static void main(String[] args) {
        SpringApplication.run(WETApplication.class, args);
    }
}
