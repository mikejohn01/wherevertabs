package com.wet.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

/* Created by MGlushkovskiy on 11.06.2021. */
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@Entity
@Table(name="link")
public class Link {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column (name="id")
    private Long id;

    @NonNull
    @Column(name="link")
    private String link;

    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;
}
