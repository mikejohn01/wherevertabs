package com.wet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.wet.model.Link;
import com.wet.model.User;
import com.wet.service.UserService;

@Controller
public class IndexController {

	@Autowired
	UserService userService;

	@GetMapping(value = "/index")
	public String printUsers(ModelMap model) {
		model.addAttribute("messages", userService.listUsers());
		return "index";
	}

	@GetMapping(value = "/new_user")
	public String newUser(ModelMap model) {
		model.addAttribute("user", new User());
		return "new_user";
	}

	@PostMapping(value = "/new_user")
	public String addUser(@RequestParam("login") String login, @RequestParam("password") String password,
						  @RequestParam("name") String name, @RequestParam("email") String email, ModelMap model) {
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		user.setName(name);
		user.setEmail(email);
		userService.add(user);
		return "redirect:/index";
	}
	//посмотреть ссыоки  конкретного пользователя
	@GetMapping(value = "links/{id}")
	public String printLinks(@PathVariable("id") Long id, Model model) {
		model.addAttribute("links", userService.listLinks(id));
		return "links";
	}
    //добавить ссылку у пользователя
	@PostMapping(value = "links/{id}")
    public String addLink(@RequestParam(value="link", required = true) String linkAdress,
                          @PathVariable("id") Long id, ModelMap model) {	// КАК сюда передать id?????????????
        Link link = new Link();
        link.setUser(userService.getUser(id));
        link.setLink(linkAdress);
        userService.addLink(link);
        return "redirect:/links/{id}";	//поменять переход на redirect:/links - как тут быть с id?
    }

	//ОТДЕЛЬНАЯ страница создания новой ссылки
	//@GetMapping(value = "{id}/new_link")
    @GetMapping(value = "/new_link")
	//public String newLink(ModelMap model, @PathVariable ("id") Long id) {
    public String newLink(ModelMap model) {
		model.addAttribute("link", new Link());
		return "new_link";
	}
	//метод сохранения новой ссылки для человека с ОТДЕЛЬНОЙ страницы
	@PostMapping(value = "/new_link")
	//public String addLinkSep (@RequestParam("link") String linkAdress, @RequestParam("id") Long id, ModelMap model) {	// КАК сюда передать id?????????????
    public String addLinkSep (@RequestParam("link") String linkAdress, ModelMap model) {	// КАК сюда передать id?????????????
		Link link = new Link();
		//link.setUser(userService.getUser(id));
		link.setLink(linkAdress);
        //link.setUser(userService.getUser(Long.valueOf(1)));

        //userService.addLink(link);
        User user = userService.getUser(Long.valueOf(1));
        link.setUser(user);
        user.addLink(link);
        userService.update(user);

		return "redirect:/new_link";	//поменять переход на redirect:/links - как тут быть с id?
	}
}