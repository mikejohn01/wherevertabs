package com.wet.dao;

import com.wet.model.Link;
import com.wet.model.User;

import java.util.List;

public interface Dao {
   void add(User user);
   void update(User user);
   List<User> listUsers();
   void drop(User user);
   User getUser(Long id);

   List<Link> listLinks(Long id);
   void addLink(Link link);
}
