package com.wet.dao;

import org.springframework.transaction.annotation.Transactional;
import com.wet.model.Link;
import com.wet.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class DaoImp implements com.wet.dao.Dao {

   @Autowired
   private SessionFactory sessionFactory;

   @Override
   public void add(User user) {
      sessionFactory.getCurrentSession().save(user);
   }

   @Override
   public void update(User user) { sessionFactory.getCurrentSession().update(user); }

   @Override
   @SuppressWarnings("unchecked")
   public List<User> listUsers() {
      TypedQuery<User> query = sessionFactory.getCurrentSession().createQuery("from User");
      return query.getResultList();
   }

   // Метод удаления Юзера
   @Override
   public void drop(User user) {
      sessionFactory.getCurrentSession().delete(user);
   }

   @Override
   public User getUser(Long id) {
      TypedQuery<User> query = sessionFactory.getCurrentSession().createQuery("from User where id =:paramName");
      query.setParameter("paramName", id);
      List <User> list = query.getResultList();
      return list.get(0);
   }

   @Override
   public List<Link> listLinks(Long id) {
      //TypedQuery<Link> query = (TypedQuery<Link>)sessionFactory.getCurrentSession().get(Link.class, id);
      TypedQuery<Link> query = sessionFactory.getCurrentSession().createQuery("from Link where user_id =:paramName");
      query.setParameter("paramName", id);
      return query.getResultList();
   }

   @Override
   public void addLink(Link link) {
      sessionFactory.getCurrentSession().save(link);
      }
   //public void addLink(Long id, String link) {sessionFactory.getCurrentSession().save(link);    }
}
