package com.wet.service;

import com.wet.dao.Dao;
import com.wet.model.Link;
import com.wet.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImp implements com.wet.service.UserService {

   @Autowired
   private Dao dao;

   @Override
   public void add(User user) {
      dao.add(user);
   }

   @Override
   public void update(User user) {
      dao.update(user);
   }

   @Override
   public List<User> listUsers() {
      return dao.listUsers();
   }

   @Override
   public void drop(User user) {
      dao.drop(user);
   }

   @Override
   public User getUser(Long id) {
      return dao.getUser(id);
   }

   @Override
   public List<Link> listLinks(Long id) {
      return dao.listLinks(id);
   }

   @Transactional
   @Override
   public void addLink(Link link) {
      dao.addLink(link);
   }
}
